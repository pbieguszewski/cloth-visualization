#ifndef CLOTH_APPLICATION_H
#define CLOTH_APPLICATION_H

#include <string>
#include <vector>

#include <vulkan/vulkan.h>

#include <GLFW/glfw3.h>

class ClothApplication
{
public:
  ClothApplication() = default;
  ~ClothApplication() noexcept { clear(); }

  void initialize();

  void render();

private:
  bool m_isInitialized = false;

  GLFWwindow* m_window = nullptr;
  VkInstance m_instance = VK_NULL_HANDLE;
  VkSurfaceKHR m_surface = VK_NULL_HANDLE;
  VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;

  std::uint32_t m_graphicsFamily = 0;
  std::uint32_t m_presentFamily = 0;

  void initializeWindow();
  void initializeVulkanInstance();
  void initializeSurface();
  void selectSuitablePhysicalDevice();

  void clear() noexcept;
  void destroyWindow() noexcept;
  void destroyVulkanInstance() noexcept;
  void destroySurface() noexcept;

  bool isSuitablePhysicalDevice(VkPhysicalDevice physicalDevice,
                                std::uint32_t* graphicsFamily,
                                std::uint32_t* presentFamily) const;

  static std::string getPhysicalDeviceName(VkPhysicalDevice physicalDevice);
  static bool checkIfPhysicalDeviceIsGPU(VkPhysicalDevice physicalDevice);
  static std::vector<std::uint32_t> getQueueIDs(
    const std::vector<VkQueueFamilyProperties>& queueFamilies,
    VkQueueFlagBits queueFlag);
  static std::vector<std::uint32_t> getQueueIDs(
    const std::vector<VkQueueFamilyProperties>& queueFamilies,
    VkPhysicalDevice device,
    VkSurfaceKHR surface);

  static VKAPI_ATTR VkBool32 VKAPI_CALL
  debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                VkDebugUtilsMessageTypeFlagsEXT messageType,
                const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                void* pUserData);
};

#endif // CLOTH_APPLICATION_H
