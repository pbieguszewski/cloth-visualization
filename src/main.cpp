#include <spdlog/spdlog.h>

#include "ClothApplication.h"

int main()
{
  spdlog::set_level(spdlog::level::trace);

  ClothApplication app;

  try
  {
    app.initialize();
  }
  catch (const std::exception& ex)
  {
    spdlog::error("Cannot initialize cloth application! What: '{0}'", ex.what());

    return EXIT_FAILURE;
  }
  catch (...)
  {
    spdlog::error("Cannot initialize cloth application! What: 'UNKNOWN'");

    return EXIT_FAILURE;
  }

  try
  {
    app.render();
  }
  catch (const std::exception& ex)
  {
    spdlog::error("Error occur during rendering cloth! What: '{0}'", ex.what());

    return EXIT_FAILURE;
  }
  catch (...)
  {
    spdlog::error("Error occur during rendering cloth! What: 'UNKNOWN'");

    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
