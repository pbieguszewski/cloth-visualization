#include "ClothApplication.h"

#include <cassert>
#include <chrono>
#include <optional>
#include <stdexcept>

#include <fmt/format.h>
#include <magic_enum.hpp>
#include <spdlog/spdlog.h>

constexpr auto WIDTH = 800;
constexpr auto HEIGHT = 600;

constexpr auto TITLE_FORMAT = "Cloth visualization {0:.2f} fps";

/*****************************/

static std::vector<const char*> getRequiredInstanceExtensions();

/*****************************/

void ClothApplication::initialize()
{
  if (m_isInitialized)
    return;

  initializeWindow();
  initializeVulkanInstance();
  initializeSurface();
  selectSuitablePhysicalDevice();

  m_isInitialized = true;
}

void ClothApplication::render()
{
  if (!m_isInitialized)
  {
    spdlog::error("Application is not initialized!");
    throw std::runtime_error("m_isInitialized");
  }

  assert(m_window);

  std::chrono::time_point<std::chrono::high_resolution_clock> delta{};
  std::size_t frameCount = 0;
  std::string title;

  while (!glfwWindowShouldClose(m_window))
  {
    const auto startFrame = std::chrono::high_resolution_clock::now();

    glfwPollEvents();

    const auto stopFrame = std::chrono::high_resolution_clock::now();

    delta += (stopFrame - startFrame);
    frameCount++;

    const auto deltaAsMillis = std::chrono::duration_cast<std::chrono::milliseconds>(
      std::chrono::time_point_cast<std::chrono::milliseconds>(delta).time_since_epoch());

    if (deltaAsMillis > std::chrono::milliseconds(1000))
    {
      const auto fps = (frameCount * 1000) / static_cast<float>(deltaAsMillis.count());

      title = fmt::format(TITLE_FORMAT, fps);

      glfwSetWindowTitle(m_window, title.c_str());

      delta = {};
      frameCount = 0;
    }
  }
}

void ClothApplication::clear() noexcept
{
  destroySurface();
  destroyVulkanInstance();
  destroyWindow();

  glfwTerminate();
}

void ClothApplication::initializeWindow()
{
  glfwSetErrorCallback(
    [](int error_code, const char* description)
    { spdlog::error("GLFW error: code '{0}', description: '{1}'", error_code, description); });

  auto res = glfwInit();
  if (res != GLFW_TRUE)
  {
    spdlog::error("Cannot initialize glfw!");
    throw std::runtime_error("glfwInit");
  }

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

  const auto startupTitle = fmt::format(TITLE_FORMAT, 0.0);

  m_window = glfwCreateWindow(WIDTH, HEIGHT, startupTitle.c_str(), nullptr, nullptr);
  if (!m_window)
  {
    spdlog::error("Cannot create window!");
    throw std::runtime_error("glfwCreateWindow");
  }

  glfwSetWindowUserPointer(m_window, this);
}

void ClothApplication::initializeVulkanInstance()
{
  VkApplicationInfo appInfo = { .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
                                .pNext = nullptr,
                                .pApplicationName = "Cloth application",
                                .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
                                .pEngineName = "No Engine",
                                .engineVersion = VK_MAKE_VERSION(1, 0, 0),
                                .apiVersion = VK_API_VERSION_1_0 };

#ifndef NDEBUG
  VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo = {
    .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
    .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                       VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                       VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
    .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                   VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                   VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
    .pfnUserCallback = debugCallback
  };
#endif

  const auto requiredInstanceExtensions = getRequiredInstanceExtensions();

  VkInstanceCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
#ifdef NDEBUG
    .pNext = nullptr,
#else
    .pNext = &debugCreateInfo,
#endif
    .flags = 0,
    .pApplicationInfo = &appInfo,
    .enabledLayerCount = 0,
    .ppEnabledLayerNames = nullptr,
    .enabledExtensionCount = static_cast<std::uint32_t>(requiredInstanceExtensions.size()),
    .ppEnabledExtensionNames = requiredInstanceExtensions.data()
  };

  const auto result = vkCreateInstance(&createInfo, nullptr, &m_instance);
  if (result != VK_SUCCESS)
  {
    spdlog::error("Cannot create vulkan instance! What: '{}'",
                  magic_enum::enum_name(result).data());
    throw std::runtime_error("vkCreateInstance");
  }
}

void ClothApplication::initializeSurface()
{
  const auto result = glfwCreateWindowSurface(m_instance, m_window, nullptr, &m_surface);

  if (result != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create window surface!");
  }
}

void ClothApplication::selectSuitablePhysicalDevice()
{
  uint32_t deviceCount = 0;
  vkEnumeratePhysicalDevices(m_instance, &deviceCount, nullptr);

  if (deviceCount == 0)
  {
    spdlog::error("Cannot find any physical devices");
    throw std::runtime_error("vkEnumeratePhysicalDevices");
  }

  auto deviceList = std::vector<VkPhysicalDevice>(deviceCount);
  vkEnumeratePhysicalDevices(m_instance, &deviceCount, deviceList.data());

  for (auto physicalDevice : deviceList)
  {
    const auto name = getPhysicalDeviceName(physicalDevice);

    spdlog::debug("Checking if {} is suitable...", name);

    std::uint32_t graphicsFamily = 0;
    std::uint32_t presentFamily = 0;
    if (isSuitablePhysicalDevice(physicalDevice, &graphicsFamily, &presentFamily))
    {
      spdlog::info("Physical device {} is fine.", name);

      m_physicalDevice = physicalDevice;
      m_graphicsFamily = graphicsFamily;
      m_presentFamily = presentFamily;

      break;
    }
    else
    {
      spdlog::debug("Physical device {} is not suitable.", name);
    }
  }

  if (m_physicalDevice == VK_NULL_HANDLE)
  {
    spdlog::error("Cannot find any suitable physical devices");
    throw std::runtime_error("isSuitablePhysicalDevice");
  }
}

void ClothApplication::destroyWindow() noexcept
{
  if (!m_window)
    return;

  glfwDestroyWindow(m_window);
  m_window = nullptr;
}

void ClothApplication::destroyVulkanInstance() noexcept
{
  if (!m_instance)
    return;

  vkDestroyInstance(m_instance, nullptr);
  m_instance = VK_NULL_HANDLE;
}

void ClothApplication::destroySurface() noexcept
{
  if (!m_surface)
    return;

  vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
  m_surface = VK_NULL_HANDLE;
}

bool ClothApplication::isSuitablePhysicalDevice(VkPhysicalDevice physicalDevice,
                                                std::uint32_t* graphicsFamily,
                                                std::uint32_t* presentFamily) const
{
  if (!checkIfPhysicalDeviceIsGPU(physicalDevice))
    return false;

  uint32_t queueFamilyCount = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

  auto queueFamilies = std::vector<VkQueueFamilyProperties>(queueFamilyCount);
  vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

  const auto graphicsQueueIDs = getQueueIDs(queueFamilies, VK_QUEUE_GRAPHICS_BIT);
  const auto presentQueueIDs = getQueueIDs(queueFamilies, physicalDevice, m_surface);

  std::optional<std::uint32_t> graphicsFam;
  std::optional<std::uint32_t> presentFam;

  if (!graphicsQueueIDs.empty())
  {
    graphicsFam = graphicsQueueIDs.front(); // this time we get first one
    *graphicsFamily = graphicsFam.value();
  }

  if (!presentQueueIDs.empty())
  {
    presentFam = presentQueueIDs.front(); // this time we get first one
    *presentFamily = presentFam.value();
  }

  return graphicsFam.has_value() && presentFam.has_value();
}

std::string ClothApplication::getPhysicalDeviceName(VkPhysicalDevice physicalDevice)
{
  VkPhysicalDeviceProperties properties = {};
  vkGetPhysicalDeviceProperties(physicalDevice, &properties);

  return std::string{ properties.deviceName };
}

bool ClothApplication::checkIfPhysicalDeviceIsGPU(VkPhysicalDevice physicalDevice)
{
  VkPhysicalDeviceProperties properties = {};
  vkGetPhysicalDeviceProperties(physicalDevice, &properties);

  return properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU ||
         properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

std::vector<std::uint32_t> ClothApplication::getQueueIDs(
  const std::vector<VkQueueFamilyProperties>& queueFamilies,
  VkQueueFlagBits queueFlag)
{
  if (queueFamilies.empty())
    return {};

  std::vector<std::uint32_t> queueIDs;
  queueIDs.reserve(queueFamilies.size());

  std::uint32_t id = 0;
  for (const auto& queueFamily : queueFamilies)
  {
    if (queueFamily.queueFlags & queueFlag)
    {
      queueIDs.push_back(id);
    }

    id++;
  }

  return queueIDs;
}

std::vector<std::uint32_t> ClothApplication::getQueueIDs(
  const std::vector<VkQueueFamilyProperties>& queueFamilies,
  VkPhysicalDevice device,
  VkSurfaceKHR surface)
{
  if (queueFamilies.empty())
    return {};

  std::vector<std::uint32_t> queueIDs;
  queueIDs.reserve(queueFamilies.size());

  std::uint32_t id = 0;
  for (const auto& queueFamily : queueFamilies)
  {
    VkBool32 presentSupport = VK_FALSE;
    vkGetPhysicalDeviceSurfaceSupportKHR(device, id, surface, &presentSupport);

    if (presentSupport)
    {
      queueIDs.push_back(id);
    }

    id++;
  }

  return queueIDs;
}

VKAPI_ATTR VkBool32 VKAPI_CALL
ClothApplication::debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                                VkDebugUtilsMessageTypeFlagsEXT messageType,
                                const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                                void* pUserDat)
{
  switch (messageSeverity)
  {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
      spdlog::debug("{}", pCallbackData->pMessage);
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
      spdlog::info("{}", pCallbackData->pMessage);
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
      spdlog::warn("{}", pCallbackData->pMessage);
      break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
      spdlog::error("{}", pCallbackData->pMessage);
      break;
    default:
      break;
  }

  return VK_FALSE;
}

/*****************************/

std::vector<const char*> getRequiredInstanceExtensions()
{
  std::uint32_t glfwExtensionCount = 0;
  auto glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

  std::vector<const char*> requiredInstanceExtensions;
  requiredInstanceExtensions.reserve(glfwExtensionCount);

  requiredInstanceExtensions.insert(
    requiredInstanceExtensions.begin(), glfwExtensions, glfwExtensions + glfwExtensionCount);

  return requiredInstanceExtensions;
}

/*****************************/
